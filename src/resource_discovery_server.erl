%%%-------------------------------------------------------------------
%%% @author antibi0tic
%%% @doc
%%% @end
%%% Created : 14. Aug 2016 8:10 PM
%%%-------------------------------------------------------------------
-module(resource_discovery_server).
-author("antibi0tic").

-behaviour(gen_server).

%% API
-export([start_link/0,
	add_resource/2,
	get_resources/1,
	get_all_resources/0]).

%% gen_server callbacks
-export([init/1,
	handle_call/3,
	handle_cast/2,
	handle_info/2,
	terminate/2,
	code_change/3]).

-define(SERVER, ?MODULE).
-define(DEFAULT_REFRESH_TIMEOUT_MILISEC, 30000). %% 30 seconds

-record(state, {local_resources = #{}, resources = #{}, refresh_timer}).

%%%===================================================================
%%% API
%%%===================================================================

start_link() ->
	gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

add_resource(Type, Resource) ->
	gen_server:cast(?SERVER, {add_resource, {Type, Resource}}).

get_resources(Type) ->
	gen_server:call(?SERVER, {get_resources, Type}).

get_all_resources() ->
	gen_server:call(?SERVER, get_all_resources).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================
%% TODO: create parameter for DEFAULT_REFRESH_TIMEOUT_SEC
%% TODO: add ability to specify requested resource types
init([]) ->
	net_kernel:monitor_nodes(true),
	Timer = erlang:send_after(?DEFAULT_REFRESH_TIMEOUT_MILISEC, self(), refresh_resources),
	{ok, #state{refresh_timer = Timer}}.

%%--------------------------------------------------------------------
%% @private
%% @doc Handling call messages
%% @end
%%--------------------------------------------------------------------
handle_call({get_resources, Type}, _From, State) ->
	Res = maps:get(Type, State#state.resources, []),
	{reply, Res, State};

handle_call(get_all_resources, _From, State) ->
	{reply, State#state.resources, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc Handling cast messages
%% @end
%%--------------------------------------------------------------------
handle_cast({add_resource, {Type, Resource}}, #state{local_resources = LocalRes, resources = Res} = State) ->
	error_logger:info_msg("Local resource added~nType: ~p~nRes: ~p~n", [Type, Resource]),
	NewLocalRes = add_resource(Type, Resource, LocalRes),
	NewRes = add_resource(Type, Resource, Res),
	{noreply, State#state{local_resources = NewLocalRes, resources = NewRes}};

handle_cast({request_resources, From}, State) ->
	gen_server:cast(From, {response_resources, State#state.local_resources}),
	{noreply, State};

handle_cast({response_resources, RemoteResources}, State) ->
	error_logger:info_msg("Remote resources added~n~p~n", [RemoteResources]),
	NewResources = merge_resources(State#state.resources, RemoteResources),
	{noreply, State#state{resources = NewResources}}.

%%--------------------------------------------------------------------
%% @private
%% @doc Handling all non call/cast messages
%% @end
%%--------------------------------------------------------------------
handle_info(refresh_resources, State) ->
	erlang:cancel_timer(State#state.refresh_timer),
	gen_server:abcast(nodes(), ?SERVER, {request_resources, self()}),
	NewTimer = erlang:send_after(?DEFAULT_REFRESH_TIMEOUT_MILISEC, self(), refresh_resources),
	{noreply, State#state{refresh_timer = NewTimer}};

handle_info({nodeup, _Node}, State) ->
	%% That's great! We aren't alone!
	{noreply, State};

handle_info({nodedown, Node}, State) ->
	%% Oh crap! We need to clean up resources that came from this node!
	%% TODO: add resource cleanup on node_down message
	error_logger:warning_msg("Node down: ~p~n", [Node]),
	{noreply, State};

handle_info(Info, State) ->
	error_logger:info_msg("Uknown message recieved: ~p~n", [Info]),
	{noreply, State}.

terminate(_Reason, State) ->
	net_kernel:monitor_nodes(false),
	erlang:cancel_timer(State#state.refresh_timer),
	ok.

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
merge_resources(ResourcesMap1, ResourcesMap2) ->
	ResMap2 = maps:to_list(ResourcesMap2),
	lists:foldl(
		fun({Key, ResourceList2}, AccMap) ->
			ResourceList1 = maps:get(Key, AccMap, []),
			NewResourceList = lists_union(ResourceList2, ResourceList1),
			maps:put(Key, NewResourceList, AccMap)
		end,
		ResourcesMap1,
		ResMap2).

lists_union(L, []) -> L;
lists_union(L, [H | T]) ->
	lists_union([H | lists:delete(H, L)], T).

add_resource(Type, Res, ResMap) ->
	OldRes = maps:get(Type, ResMap, []),
	maps:put(Type, [Res | lists:delete(Res, OldRes)], ResMap).