%%%-------------------------------------------------------------------
%%% @author antibi0tic
%%% @doc Resource discovery system
%%%
%%% @end
%%% Created : 14. Aug 2016 7:53 PM
%%%-------------------------------------------------------------------
-module(resource_discovery).
-author("antibi0tic").

-behaviour(application).

%% Application callbacks
-export([start/2,
	stop/1]).

%% API
-export([
	add_resource/2,
	get_resources/1,
	get_all_resources/0
]).

%%%===================================================================
%%% API
%%%===================================================================
add_resource(Type, Resource) ->
	resource_discovery_server:add_resource(Type, Resource).

get_resources(Type) ->
	resource_discovery_server:get_resources(Type).

get_all_resources() ->
	resource_discovery_server:get_all_resources().

%%%===================================================================
%%% Application callbacks
%%%===================================================================

start(_StartType, _StartArgs) ->
	case resource_discovery_sup:start_link() of
		{ok, Pid} ->
			{ok, Pid};
		Error ->
			Error
	end.

stop(_State) ->
	ok.

%%%===================================================================
%%% Internal functions
%%%===================================================================
