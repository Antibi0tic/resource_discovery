%%%-------------------------------------------------------------------
%%% @author antibi0tic
%%% @doc main supervisor for resource_discovery application.
%%% @end
%%% Created : 14. Aug 2016 7:55 PM
%%%-------------------------------------------------------------------
-module(resource_discovery_sup).
-author("antibi0tic").

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API functions
%%%===================================================================

start_link() ->
	supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

init([]) ->
	SupFlags = {one_for_one, 10, 60},
	ResourceServer = {resource_discovery_server, {resource_discovery_server, start_link, []},
		permanent, 2000, worker, [resource_discovery_server]},

	{ok, {SupFlags, [ResourceServer]}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
